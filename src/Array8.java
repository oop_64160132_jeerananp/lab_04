import java.util.Scanner;
import java.util.Arrays;

public class Array8 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        int Sum = 0;

        for (int i = 0; i < 3; i++) {
            Scanner Int1 = new Scanner(System.in);
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = Int1.nextInt();
            Sum = Sum + arr[i];
        }
        System.out.print("arr = ");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("sum = " + Sum);
        double Sum1 = Sum;
        double Avg = Sum1 / arr.length;
        System.out.println("avg = " + Avg);
        int min = Arrays.stream(arr).min().getAsInt();
        System.out.println("min = " + min);
        int max = Arrays.stream(arr).max().getAsInt();
        System.out.println("max = " + max);
    }
}
