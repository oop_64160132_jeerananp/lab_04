import java.util.Scanner;
import java.lang.Math;

public class Array11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[5];
        int Num1, Num2;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }

        while (true) {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();

            System.out.print("Please input index: ");
            Num1 = sc.nextInt();
            Num2 = sc.nextInt();

            int Number = arr[Num1];
            arr[Num1] = arr[Num2];
            arr[Num2] = Number;

            boolean Finish = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    Finish = false;
                }
            }
            if (Finish) {
                for (int i = 0; i < arr.length; i++) {
                    System.out.print(arr[i] + " ");
                }
                System.out.println();
                System.out.println("You win!!!");
                break;
            }
        }
    }
}
