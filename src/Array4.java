import java.util.Scanner;

public class Array4 {
    public static void main(String[] args) {
        int arr[] = new int[3];

        for (int i = 0; i < 3; i++) {
            Scanner Int1 = new Scanner(System.in);
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = Int1.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 2; i >= 0; i--) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }
}
