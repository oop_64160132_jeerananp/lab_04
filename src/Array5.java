import java.util.Scanner;

public class Array5 {
    public static void main(String[] args) {
        int arr[] = new int[3];
        int Sum = 0;
        for (int i = 0; i < 3; i++) {
            Scanner Int1 = new Scanner(System.in);
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = Int1.nextInt();
            Sum = Sum + arr[i];
        }
        System.out.print("arr = ");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("sum = " + Sum);
    }
}
