import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        int arr[] = new int[3];

        for (int i = 0; i < 3; i++) {
            Scanner Int1 = new Scanner(System.in);
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = Int1.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0; i < 3; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        Scanner Input = new Scanner(System.in);
        System.out.print("please input search value: ");
        int Number1 = Input.nextInt();
        int index = -1;
        for (int i = 0; i < 3; i++) {
            if (arr[i] == Number1) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            System.out.println("found at index: " + index);
        } else {
            System.out.println("not found");
        }
    }
}
