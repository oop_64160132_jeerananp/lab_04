import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        Scanner Bt = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        int NumElement = Bt.nextInt();
        int arr[] = new int[NumElement];
        int Array1[];
        int Index = 0;

        Array1 = new int[NumElement];

        for (int i = 0; i < NumElement; i++) {
            Scanner Int1 = new Scanner(System.in);
            System.out.print("Element " + i + ": ");
            arr[i] = Int1.nextInt();
            int Num = -1;
            for (int j = 0; j < Index; j++) {
                if (Array1[j] == arr[i]) {
                    Num = j;
                }
            }
            if (Num < 0) {
                Array1[Index] = arr[i];
                Index++;
            }
        }
        System.out.print("All number: ");
        for (int i = 0; i < Index; i++) {
            System.out.print(Array1[i] + " ");
        }
    }
}
